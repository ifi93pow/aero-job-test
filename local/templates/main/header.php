<?php
global $APPLICATION, $USER;
?>
<!DOCTYPE html>
<html lang="<?= LANGUAGE_ID ?>">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?= $APPLICATION->ShowTitle() ?></title>

    <?Bitrix\Main\Page\Asset::getInstance()->addCss('/css/bootstrap.min.css')?>
    <?Bitrix\Main\Page\Asset::getInstance()->addCss('/css/app.css')?>
    <!--<link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/app.css" rel="stylesheet">-->

    <?Bitrix\Main\Page\Asset::getInstance()->addJs('/js/jquery-1.11.3.min.js')?>
    <?Bitrix\Main\Page\Asset::getInstance()->addJs('/js/bootstrap.min.js')?>

    <? $APPLICATION->ShowHead() ?>
</head>
<body>
<? $APPLICATION->ShowPanel() ?>
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">Тестовое задание AERO</a>
        </div>
        <?/*<div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <!--<li class="active"><a href="/"></a></li>-->
                <li><a href="/2-list/">Задание 2 (list)</a></li>
            </ul>
        </div><!--/.nav-collapse -->*/?>
    </div>
</nav>

<div class="container">

    <div class="starter-template">
        <?/*$APPLICATION->IncludeComponent(
            "bitrix:breadcrumb",
            "",
            Array(
                "PATH" => "",
                "SITE_ID" => "s1",
                "START_FROM" => "0"
            )
        );*/?>
        <h1><?= $APPLICATION->ShowTitle(false) ?></h1>
