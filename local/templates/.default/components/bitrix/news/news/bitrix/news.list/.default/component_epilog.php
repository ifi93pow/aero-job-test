<?php
$APPLICATION->SetTitle($arResult['TITLE']);

$html = preg_replace_callback('/#LIKES_(\d+)#/', function ($s) {
    global $APPLICATION;
    ob_start();
    $APPLICATION->IncludeComponent('app:like', '', array('ID' => $s[1]));
    return ob_get_clean();
}, $arResult['HTML']);

echo $html;