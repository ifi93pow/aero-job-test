(function () {
    var disabled = false;

    $(document).on('click', '.js-user-like__button', function (e) {
        e.preventDefault(true);

        var $this = $(this);
        if ($this.hasClass('user-like__button--liked')) return;

        if (disabled) return;
        disabled = true;

        $.ajax({
            url: $this.data('url'),
            type: 'post',
            dataType: 'json',
            success: function (result) {
                $this.addClass('user-like__button--liked');
                var count = 0;
                if (typeof result['likes'][$this.data('type')] !== 'undefined' && typeof result['likes'][$this.data('type')]['VALUE'] !== 'undefined') {
                    count = result['likes'][$this.data('type')]['VALUE'];
                }

                $this.find('.js-user-like__count').attr('data-count', count);

                if (typeof result['error'] !== 'undefined') {
                    alert(result['error']);
                }
            },
            error: function (xhr, resp, text) {
                alert(xhr.responseText);
            },
            finally: function () {
                disabled = false;
            }
        });

        return false;
    });
})();