<?
$data = [
    'LIKE' => ['class' => 'like', 'name' => 'Нравится'],
    'DISLIKE' => ['class' => 'dislike', 'name' => 'Не нравится'],
]
?>
<div class="user-like">
    <? foreach ($arParams['TYPES'] as $type) { ?>
        <div
            class="js-user-like__button user-like__button user-like__button--<?= $data[$type]['class'] ?><?
            if (!empty($arResult['USER_LIKES'][$type])) { ?> user-like__button--liked<? } ?>"
            data-type="<?= $type ?>"
            data-url="/ajax/like.php?type=<?= $type ?>&id=<?= urlencode($arParams['ID']) ?>">
            <?= $data[$type]['name'] ?>
            <span
                class="user-like__count js-user-like__count"
                data-count="<?= $arResult['LIKES'][$type]['VALUE'] ?>"></span>
        </div>
    <? } ?>
</div>
