<?php

/**
 * @property \Bitrix\Main\Context $context
 * @property \Bitrix\Main\Server $server
 */
class CAppLike extends CBitrixComponent
{
    public function __construct($component)
    {
        parent::__construct($component);

        \Bitrix\Main\Loader::includeModule('app');

        $this->context = \Bitrix\Main\Application::getInstance()->getContext();
        $this->server = $this->context->getServer();
    }

    public function onPrepareComponentParams($arParams)
    {
        global $APPLICATION;

        $arParams['IS_AJAX'] = $this->request->isAjaxRequest();
        $arParams['USER_IP'] = $_SERVER['REMOTE_ADDR'];
        $arParams['TYPES'] = array('LIKE', 'DISLIKE');
        $arParams['TYPES_COUNT'] = count($arParams['TYPES']);

        if (empty($arParams['ID'])) $arParams['ID'] = $APPLICATION->GetCurPage(false);
        if (empty($arParams['TYPE']) || !in_array($arParams['TYPE'], $arParams['TYPES'])) {
            $arParams['TYPE'] = reset($arParams['TYPES']);
        }

        return $arParams;
    }

    public function executeComponent()
    {
        $this->popUserLikes();

        if ($this->startResultCache(3600000, false, SITE_ID . '/app/like/' . sha1($this->arParams['ID']) . '/')) {
            $this->popLikes();

            $this->setResultCacheKeys(['LIKES', 'USER_LIKES']);
            $this->endResultCache();
        }

        if ($this->arParams['HANDLER']) {
            $this->tryHandler();
            return;
        }

        $this->includeComponentTemplate();
    }

    function popLikes()
    {
        $likes = \App\Registry::getHighloadIBInstanceByCode('Likes');
        $rs = $likes->getList(array(
            'filter' => array('UF_URL' => $this->arParams['ID']),
            'select' => array('ID', 'UF_TYPE', 'UF_VALUE'),
            'limit' => $this->arParams['TYPES_COUNT']
        ));
        while ($ar = $rs->fetch()) {
            $this->arResult['LIKES'][$ar['UF_TYPE']] = [
                'ID' => intval($ar['ID']),
                'VALUE' => intval($ar['UF_VALUE'])
            ];
        }
    }

    function popUserLikes()
    {
        $userLikes = \App\Registry::getHighloadIBInstanceByCode('UserLikes');
        $rs = $userLikes->getList(array(
            'filter' => array('UF_IP' => $this->arParams['USER_IP'], 'UF_URL' => $this->arParams['ID']),
            'select' => array('ID', 'UF_TYPE'),
            'limit' => $this->arParams['TYPES_COUNT']
        ));
        if ($ar = $rs->fetch()) {
            $this->arResult['USER_LIKES'][$ar['UF_TYPE']] = true;
        }
    }

    function tryHandler()
    {
        global $APPLICATION;

        try {
            $result = array('likes' => &$this->arResult['LIKES'], 'userLiked' => &$this->arResult['USER_LIKES']);
            $this->handler();
        } catch (Exception $e) {
            $result['error'] = $e->getMessage();// . $e->getTraceAsString();
        }

        $APPLICATION->RestartBuffer();
        header('Content-Type: application/json');

        echo json_encode($result);
        die();
    }

    function handler()
    {
        $likes = \App\Registry::getHighloadIBInstanceByCode('Likes');
        $userLikes = \App\Registry::getHighloadIBInstanceByCode('UserLikes');

        if (!empty($this->arResult['USER_LIKES'])) throw new Exception ('Вы уже голосовали за эту страницу');

        if (empty($this->arResult['LIKES'][$this->arParams['TYPE']])) {
            $this->arResult['LIKES'][$this->arParams['TYPE']]['VALUE'] = 1;
            $likes->add(array(
                'UF_URL' => $this->arParams['ID'],
                'UF_TYPE' => $this->arParams['TYPE'],
                'UF_VALUE' => $this->arResult['LIKES'][$this->arParams['TYPE']]['VALUE']
            ));
        } else {
            $this->arResult['LIKES'][$this->arParams['TYPE']]['VALUE']++;
            $likes->update($this->arResult['LIKES'][$this->arParams['TYPE']]['ID'], array(
                'UF_VALUE' => $this->arResult['LIKES'][$this->arParams['TYPE']]['VALUE']
            ));
        }

        $arFields = array(
            'UF_URL' => $this->arParams['ID'],
            'UF_TYPE' => $this->arParams['TYPE'],
            'UF_IP' => $this->arParams['USER_IP'],
            'UF_TIME' => FormatDate('FULL', time())
        );

        $r = $userLikes->add($arFields);
        $this->clearCache();
    }

    function clearCache()
    {
        BXClearCache($this->getCachePath());
    }
}