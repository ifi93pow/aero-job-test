<?php


namespace App\Migrations;

use Bitrix\Main\Entity\Query;
use App\Registry;

abstract class AbstractMigration
{
    /**
     * @var DataGenerator
     */
    public $dataGenerator;

    abstract function run();

    function __construct()
    {
        \Bitrix\Main\Loader::includeModule('iblock');
        $this->dataGenerator = new DataGenerator();
    }

    function isExistIblockType($iblockType)
    {
        $query = new Query(\Bitrix\Iblock\TypeTable::getEntity());
        $query->setFilter(['ID' => $iblockType]);
        return $query->exec()->getSelectedRowsCount() > 0;
    }

    function createIblockType($iblockType, $arFields)
    {
        if ($this->isExistIblockType($iblockType)) throw new \Exception("iblockType $iblockType already exists");

        $arFieldsDefault = [
            'ID' => $iblockType,
            'SECTIONS' => 'Y',
            'IN_RSS' => 'N',
            'SORT' => 100,
        ];

        $arFields = array_merge($arFieldsDefault, $arFields);

        $name = $arFields['NAME'];
        unset($arFields['NAME']);

        $oResult = \Bitrix\Iblock\TypeTable::add($arFields);
        $arResultData = $oResult->getData();

        if ($name) {
            foreach ($this->getLangs() as $arLang) {
                $arFieldsIblockTypeLang = array(
                    'IBLOCK_TYPE_ID' => $arResultData['ID'],
                    'LANGUAGE_ID' => $arLang['ID'],
                    'NAME' => $name,
                );
                $oResult2 = \Bitrix\Iblock\TypeLanguageTable::add($arFieldsIblockTypeLang);
            }
        }

        return $oResult;
    }

    function getLangs()
    {
        $query = new Query(\Bitrix\Main\Localization\LanguageTable::getEntity());
        $result = $query->setSelect(['ID'])->exec()->fetchAll();
        return $result;
    }

    function getSitesIds()
    {
        $query = new Query(\Bitrix\Main\SiteTable::getEntity());
        $result = $query->setSelect(['LID'])->exec()->fetchAll();
        $result = array_map(function ($a) {
            return $a['LID'];
        }, $result);
        return $result;
    }

    function isExistIblockCode($iblockCode)
    {
        $query = new Query(\Bitrix\Iblock\IblockTable::getEntity());
        $query->setFilter(['CODE' => $iblockCode]);
        return $query->exec()->getSelectedRowsCount() > 0;
    }

    function createIblockCode($iblockCode, $arFields)
    {
        if ($this->isExistIblockCode($iblockCode)) throw new \Exception("$iblockCode already exists");

        $arFieldsDefault = [
            'CODE' => $iblockCode,
            'IBLOCK_TYPE_ID' => $iblockCode,
            'NAME' => 'Инфоблок',
            'SORT' => 100,
            'LIST_PAGE_URL' => '#SITE_DIR#/',
            'SECTION_PAGE_URL' => '#SITE_DIR#/#SECTION_CODE#/',
            'DETAIL_PAGE_URL' => '#SITE_DIR#/#SECTION_CODE#/#ELEMENT_CODE#/',
            'INDEX_ELEMENT' => 'Y',
            'INDEX_SECTION' => 'Y',
            'VERSION' => 1,
        ];

        $arFields = array_merge($arFieldsDefault, $arFields);

        $arSiteId = $arFields['SITE_ID'];
        unset($arFields['SITE_ID']);
        if ($arSiteId && !is_array($arSiteId)) {
            $arSiteId = [$arSiteId];
        } else {
            $arSiteId = $this->getSitesIds();
        }
        if (empty($arFields['LID'])) {
            $arFields['LID'] = reset($arFields['LID']);
        }

//        $oResult = \Bitrix\Iblock\IblockTable::add($arFields);
//        $iblockId = $oResult->getId();
//
//        foreach ($arSiteId as $sSiteId) {
//            $arFieldsIblockSite = array(
//                'IBLOCK_ID' => $iblockId,
//                'SITE_ID' => $sSiteId,
//            );
//            $oResult2 = \Bitrix\Iblock\IblockSiteTable::add($arFieldsIblockSite);
//        }

//        return $oResult;

        $arFields['SITE_ID'] = $arSiteId;
        $ob = new \CIBlock();
        $id = $ob->Add($arFields);
        if ($ob->LAST_ERROR) throw new \Exception($ob->LAST_ERROR);
        return $id;
    }

    function isExistIblockElement($arFields)
    {
        unset($arFields['PROPERTY_VALUES']);
        $query = new Query(\Bitrix\Iblock\ElementTable::getEntity());
        $query->setFilter($arFields);
        return $query->exec()->getSelectedRowsCount() > 0;
    }

    function createIblockElement($iblockCode, $arFields)
    {
        $arFields['IBLOCK_ID'] = Registry::getIblockIdByCode($iblockCode);
        if ($this->isExistIblockElement($arFields)) throw new \Exception("element already exists");

        // Для добавления элементов инфоблоков используйте вызов CIBlockElement::Add()
//        $oResult = \Bitrix\Iblock\ElementTable::add($arFields);
//        return $oResult;
        $ob = new \CIBlockElement();
        $id = $ob->Add($arFields);
        if ($ob->LAST_ERROR) throw new \Exception($ob->LAST_ERROR);
        return $id;
    }


    function isExistUser($arFields)
    {
        $query = new Query(\Bitrix\Main\UserTable::getEntity());
        $query->setFilter($arFields);
        return $query->exec()->getSelectedRowsCount() > 0;
    }

    function createUser($arFields)
    {
        if ($this->isExistUser($arFields)) throw new \Exception("user already exists");

        // Use CUser class
//        $oResult = \Bitrix\Main\UserTable::add($arFields);
//        $iblockId = $oResult->getId();
//        return $oResult;
        $ob = new \CUser();
        $id = $ob->Add($arFields);
        if ($ob->LAST_ERROR) throw new \Exception($ob->LAST_ERROR);
        return $id;
    }

    function isExistIblockProperty($arFields)
    {
        $query = new Query(\Bitrix\Iblock\PropertyTable::getEntity());
        $query->setFilter(['CODE' => $arFields['CODE'], 'IBLOCK_ID' => $arFields['IBLOCK_ID']]);
        return $query->exec()->getSelectedRowsCount() > 0;
    }

    function createIblockProperty($iblockCode, $arFields)
    {
        $arFieldsDefault = Array(
            'NAME' => 'Свойство',
            'ACTIVE' => 'Y',
            'SORT' => '100',
            'CODE' => 'COLOR',
            'PROPERTY_TYPE' => 'S',
            'MULTIPLE' => 'N',
            'IBLOCK_ID' => Registry::getIblockIdByCode($iblockCode)
        );

        $arFields = array_merge($arFieldsDefault, $arFields);

        if ($this->isExistIblockProperty($arFields)) throw new \Exception("property already exists");

        $ob = new \CIBlockProperty();
        $id = $ob->Add($arFields);
        if ($ob->LAST_ERROR) throw new \Exception($ob->LAST_ERROR);
        return $id;
    }

    function clearCache()
    {
        BXClearCache(true);

        $obCache = \Bitrix\Main\Data\Cache::createInstance();
        $obCache->cleanDir(false, "managed_cache");

        $taggedCache = \Bitrix\Main\Application::getInstance()->getTaggedCache();
        $taggedCache->clearByTag(true);
    }

    function createHLIblock($arFields)
    {
        $result = \Bitrix\Highloadblock\HighloadBlockTable::add($arFields);
        return $result;
    }

    function createHLIblockProperty($hliblockId, $name, $arFields)
    {
        $langs = $this->getLangs();
        $labelKeys = ['EDIT_FORM_LABEL', 'LIST_COLUMN_LABEL', 'LIST_FILTER_LABEL'];

        foreach ($labelKeys as $key) {
            foreach ($langs as $lang) {
                $arFields[$key][$lang['ID']] = $name;
            }
        }

        $arFields['ENTITY_ID'] = 'HLBLOCK_' . $hliblockId;

        $ob = new \CUserTypeEntity();
        $id = $ob->Add($arFields);
        if ($ob->LAST_ERROR) throw new \Exception($ob->LAST_ERROR);
        return $id;
    }
}