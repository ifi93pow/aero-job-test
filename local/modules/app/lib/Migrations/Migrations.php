<?php

namespace App\Migrations;

class Migrations
{
    private function __construct()
    {
        \Bitrix\Main\Loader::includeModule('iblock');
        \Bitrix\Main\Loader::includeModule('highloadblock');
    }

    /**
     * @return static
     */
    static function instance() {
        static $instance;
        if(!isset($instance)) {
            $instance = new static();
        }
        return $instance;
    }

    public function run($migrationFile) {
        /**
         * @var \App\Migrations\AbstractMigration $migration
         */
        $beforeClassesCount = count(get_declared_classes());
        require $migrationFile;
        $afterClasses = get_declared_classes();
        $afterClassesCount = count($afterClasses);
        for ($i = $beforeClassesCount; $i < $afterClassesCount; $i++) {
            if (is_subclass_of($afterClasses[$i], '\App\Migrations\AbstractMigration')) {
                $migration = new $afterClasses[$i]();
                $migration->run();
            }
        }
    }
}