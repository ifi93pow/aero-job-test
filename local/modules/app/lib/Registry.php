<?php

namespace App;

use Bitrix\Main\Loader;

class Registry
{
    static function getIblockIdByCode($iblockCode, $bRenew = false)
    {
        static $result = array();
        if (!isset($result[$iblockCode]) || $bRenew) {
            $arResult = \Bitrix\Iblock\IblockTable::getList([
                'select' => ['ID', 'CODE']
            ])->fetchAll();
            foreach ($arResult as $arItem) {
                $result[$arItem['CODE']] = $arItem['ID'];
            }
        }
        return $result[$iblockCode];
    }

    /**
     * @return \Bitrix\Main\Entity\DataManager
     */
    static function getHighloadIBInstance($hlIblockId)
    {
        if (!$hlIblockId) throw new \InvalidArgumentException('hlIblockId empty');
        static $result = array();
        if (!isset($result[$hlIblockId])) {
            \Bitrix\Main\Loader::includeModule("highloadblock");

            $hlblock = \Bitrix\Highloadblock\HighloadBlockTable::getById($hlIblockId)->fetch();
            $entity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
            $entity_data_class = $entity->getDataClass();

            $result[$hlIblockId] = new $entity_data_class();
        }
        return $result[$hlIblockId];
    }

    /**
     * @return \Bitrix\Main\Entity\DataManager
     */
    static function getHighloadIBInstanceByCode($code)
    {
        return self::getHighloadIBInstance(self::getHighloadIBIdByCode($code));
    }

    static function getHighloadIBIdByCode($code)
    {
        static $result;
        if (!isset($result)) {
            \Bitrix\Main\Loader::includeModule("highloadblock");
            $result = [];
            $rs = \Bitrix\Highloadblock\HighloadBlockTable::getList(['select' => ['ID', 'NAME']]);
            while ($ar = $rs->fetch()) {
                $result[$ar['NAME']] = $ar['ID'];
            }
        }
        return $result[$code];
    }
}