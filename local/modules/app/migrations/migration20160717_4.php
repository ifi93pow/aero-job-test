<?php

class Migration20160717_4 extends \App\Migrations\AbstractMigration
{
    function run()
    {
        $result = $this->createHLIblock(array(
            'NAME' => 'Likes',
            'TABLE_NAME' => 'likes',
        ));

        $hlIblockId = $result->getId();


        $this->createHLIblockProperty($hlIblockId, 'Идентификатор страницы', array(
            'USER_TYPE_ID' => 'string',
            'FIELD_NAME' => 'UF_URL',
        ));

        $this->createHLIblockProperty($hlIblockId, 'Тип лайка', array(
            'USER_TYPE_ID' => 'string',
            'FIELD_NAME' => 'UF_TYPE',
        ));

        $this->createHLIblockProperty($hlIblockId, 'Количество лайков', array(
            'USER_TYPE_ID' => 'string',
            'FIELD_NAME' => 'UF_VALUE',
        ));
    }
}