<?php

class Migration20160717_3 extends \App\Migrations\AbstractMigration
{
    function run()
    {
        $result = $this->createHLIblock(array(
            'NAME' => 'UserLikes',
            'TABLE_NAME' => 'user_likes',
        ));

        $hlIblockId = $result->getId();

        
        $this->createHLIblockProperty($hlIblockId, 'Идентификатор страницы', array(
            'USER_TYPE_ID' => 'string',
            'FIELD_NAME' => 'UF_URL',
        ));

        $this->createHLIblockProperty($hlIblockId, 'Тип лайка', array(
            'USER_TYPE_ID' => 'string',
            'FIELD_NAME' => 'UF_TYPE',
        ));

        $this->createHLIblockProperty($hlIblockId, 'Айпи пользователя', array(
            'USER_TYPE_ID' => 'string',
            'FIELD_NAME' => 'UF_IP',
        ));

        $this->createHLIblockProperty($hlIblockId, 'Время', array(
            'USER_TYPE_ID' => 'date',
            'FIELD_NAME' => 'UF_TIME',
        ));
    }
}