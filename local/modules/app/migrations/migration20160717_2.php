<?php

class Migration20160717_2 extends \App\Migrations\AbstractMigration
{
    function run()
    {
        $this->createIblockCode('news', [
            'IBLOCK_TYPE_ID' => 'news',
            'NAME' => 'Новости',
            'LIST_PAGE_URL' => '#SITE_DIR#/',
            'SECTION_PAGE_URL' => '#SITE_DIR#/',
            'DETAIL_PAGE_URL' => '#SITE_DIR#/#ELEMENT_CODE#/',
        ]);

        foreach ($this->dataGenerator->arNouns as $i => $sName) {
            $this->createIblockElement('news', [
                'NAME' => "{$this->dataGenerator->arAdjective[$i]} $sName",
                'DETAIL_TEXT' => str_repeat("{$this->dataGenerator->arAdjective[$i]} $sName ", 50),
            ]);
        }
    }
}