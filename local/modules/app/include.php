<?php

use Bitrix\Main\Loader;

Loader::registerAutoLoadClasses(
    'app',
    array(
        'App\Migrations\AbstractMigration' => 'lib/Migrations/AbstractMigration.php',
        'App\Migrations\DataGenerator' => 'lib/Migrations/DataGenerator.php',
        'App\Migrations\Migrations' => 'lib/Migrations/Migrations.php',
        'App\App' => 'lib/App.php',
        'App\Registry' => 'lib/Registry.php',
    )
);